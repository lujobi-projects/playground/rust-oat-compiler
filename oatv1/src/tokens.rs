use std::char::from_u32;

use logos::{Lexer, Logos};

use crate::utils::{location::Location, range::Range};

#[derive(Logos, Clone, Debug, PartialEq)]
#[logos(skip r"[ \t\n\f]+")]
pub enum OatToken {
    #[regex("[0-9]+", |lex| lex.slice().parse().ok())]
    Int(i64),

    #[regex(r"(true|false)", bool_value)]
    Bool(bool),

    #[regex(r#""([^"\\\n]|\\.|\\\n)*""#, string_value)]
    String(String),

    #[regex(r"[a-zA-Z_]\w*", |lex| lex.slice().to_string())]
    Ident(String),

    #[regex(r#"//[^\n\r]+"#)]
    Comment,

    #[token("/*", multiline_comment)]
    MultilineComment,

    #[token("null")]
    Null,

    #[token("int")]
    TInt,

    #[token("bool")]
    TBool,

    #[token("void")]
    TVoid,

    #[token("string")]
    TString,

    #[token("if")]
    If,

    #[token("else")]
    Else,

    #[token("while")]
    While,

    #[token("for")]
    For,

    #[token("return")]
    Return,

    #[token("var")]
    Var,

    #[token(";")]
    Semi,

    #[token(",")]
    Comma,

    #[token("{")]
    LBrace,

    #[token("}")]
    RBrace,

    #[token("+")]
    Plus,

    #[token("-")]
    Dash,

    #[token("*")]
    Star,

    #[token("&")]
    LAnd,

    #[token("|")]
    LOr,

    #[token("[&]")]
    BAnd,

    #[token("[|]")]
    BOr,

    #[token("<<")]
    Shl,

    #[token(">>")]
    Shr,

    #[token(">>>")]
    Shra,

    #[token("<")]
    Lt,

    #[token("<=")]
    Lte,

    #[token(">")]
    Gt,

    #[token(">=")]
    Gte,

    #[token("!=")]
    Neq,

    #[token("==")]
    Eqeq,

    #[token("=")]
    Eq,

    #[token("(")]
    LParen,

    #[token(")")]
    RParen,

    #[token("[")]
    LBracket,

    #[token("]")]
    RBracket,

    #[token("~")]
    Tilde,

    #[token("!")]
    Bang,

    #[token("global")]
    Global,

    #[token("new")]
    New,

    Eof,
}

fn multiline_comment(lex: &mut Lexer<OatToken>) -> logos::Skip {
    let start: &str = "/*";
    let end: &str = "*/";
    let mut depth = 1;
    let mut ind = 0;

    let rem = lex.remainder().to_string();
    let mut starts = rem.match_indices(start).into_iter().peekable();
    let mut ends = rem.match_indices(end).into_iter().peekable();

    while depth > 0 {
        if let Some(end) = ends.clone().peek() {
            if let Some(start) = starts.clone().peek() {
                if start.0 < end.0 {
                    depth += 1;
                    starts.next();
                } else {
                    depth -= 1;
                    ind = ends.next().unwrap().0;
                }
            } else {
                depth -= 1;
                ind = ends.next().unwrap().0;
            }
        } else {
            panic!(
                "Unclosed comment starting with: < /*{} >",
                String::from(&rem[..(if rem.len() < 20 { rem.len() } else { 20 })])
            );
        }
    }

    lex.bump(ind + 2);
    logos::Skip
}

fn string_value(lex: &mut Lexer<OatToken>) -> Option<String> {
    let s = lex.slice();
    let mut stream = s[1..(s.len() - 1)].chars().peekable();
    let mut result = String::new();

    while let Some(next) = stream.next() {
        match next {
            '\\' => {
                if let Some(next) = stream.next() {
                    if next.is_numeric() {
                        let mut num = String::new();
                        num.push(next);
                        for _ in 0..2 {
                            if let Some(next) = stream.next() {
                                if next.is_ascii_digit() {
                                    num.push(next);
                                } else {
                                    panic!("Invalid escape sequence for unicode character: needed 3 digits");
                                }
                            } else {
                                panic!("Invalid escape sequence for unicode character: needed 3 digits");
                            }
                        }
                        let num = num.parse::<u32>().unwrap();
                        result.push(from_u32(num).unwrap());
                    } else {
                        match next {
                            'n' => result.push('\n'),
                            'r' => result.push('\r'),
                            't' => result.push('\t'),
                            '\\' => result.push('\\'),
                            '"' => result.push('"'),
                            _ => panic!("Invalid escape sequence: \\{}", next),
                        }
                    }
                } else {
                    panic!("Unclosed string literal");
                }
            }
            _ => result.push(next),
        }
    }

    Some(result)
}

fn bool_value(lex: &mut Lexer<OatToken>) -> Option<bool> {
    let slice = lex.slice();
    Some(slice == "true")
}

impl std::fmt::Display for OatToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            // OatToken::EOF => write!(f, "EOF"),
            OatToken::Int(value) => write!(f, "INT({})", value),
            OatToken::Bool(value) => write!(f, "BOOL({})", value),
            OatToken::String(value) => write!(f, "STRING({})", value),
            OatToken::Ident(value) => write!(f, "IDENT({})", value),
            OatToken::Comment => write!(f, "COMMENT"),
            OatToken::MultilineComment => write!(f, "MULTILINE_COMMENT"),
            OatToken::Null => write!(f, "NULL"),
            OatToken::TInt => write!(f, "TINT"),
            OatToken::TBool => write!(f, "TBOOL"),
            OatToken::TVoid => write!(f, "TVOID"),
            OatToken::TString => write!(f, "TSTRING"),
            OatToken::If => write!(f, "IF"),
            OatToken::Else => write!(f, "ELSE"),
            OatToken::While => write!(f, "WHILE"),
            OatToken::For => write!(f, "FOR"),
            OatToken::Return => write!(f, "RETURN"),
            OatToken::Var => write!(f, "VAR"),
            OatToken::Semi => write!(f, "SEMI"),
            OatToken::Comma => write!(f, "COMMA"),
            OatToken::LBrace => write!(f, "LBRACE"),
            OatToken::RBrace => write!(f, "RBRACE"),
            OatToken::Plus => write!(f, "PLUS"),
            OatToken::Dash => write!(f, "DASH"),
            OatToken::Star => write!(f, "STAR"),
            OatToken::LAnd => write!(f, "LAND"),
            OatToken::LOr => write!(f, "LOR"),
            OatToken::BAnd => write!(f, "BAND"),
            OatToken::BOr => write!(f, "BOR"),
            OatToken::Shl => write!(f, "SHL"),
            OatToken::Shr => write!(f, "SHR"),
            OatToken::Shra => write!(f, "SHRA"),
            OatToken::Lt => write!(f, "LT"),
            OatToken::Lte => write!(f, "LTE"),
            OatToken::Gt => write!(f, "GT"),
            OatToken::Gte => write!(f, "GTE"),
            OatToken::Neq => write!(f, "NEQ"),
            OatToken::Eqeq => write!(f, "EQEQ"),
            OatToken::Eq => write!(f, "EQ"),
            OatToken::LParen => write!(f, "LPAREN"),
            OatToken::RParen => write!(f, "RPAREN"),
            OatToken::LBracket => write!(f, "LBRACKET"),
            OatToken::RBracket => write!(f, "RBRACKET"),
            OatToken::Tilde => write!(f, "TILDE"),
            OatToken::Bang => write!(f, "BANG"),
            OatToken::Global => write!(f, "GLOBAL"),
            OatToken::New => write!(f, "NEW"),
            OatToken::Eof => write!(f, "EOF"),
        }
    }
}

pub fn make_tokenizer(source: &str) -> impl Iterator<Item = LexResult> + '_ {
    // make_tokenizer_located(source, Location::new(0, 0))
    OatLexer::new(source)
}

pub type Spanned = (Location, OatToken, Location);
pub type LexResult = Result<Spanned, LexicalError>;

#[derive(Debug, Clone)]
pub enum LexicalError {
    // UnrecognizedToken(Range),
    // UnexpectedToken(Range, char),
    UnbalancedBrackets(Range, i32),
    // Not possible
}

impl std::fmt::Display for LexicalError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            // LexicalError::UnrecognizedToken(loc) => write!(f, "Unrecognized token at {loc}"),
            // LexicalError::UnexpectedToken(loc, c) => {
            //     write!(f, "Unexpected token '{c}' at {loc}")
            // }
            LexicalError::UnbalancedBrackets(loc, n) => {
                write!(f, "Unbalanced brackets at {loc} (n={n})")
            }
        }
    }
}

pub struct OatLexer<'source> {
    internal_lexer: logos::SpannedIter<'source, OatToken>,
    nesting: i32,
    eof_reached: bool,
    newlines: Vec<usize>,
}

impl<'source> OatLexer<'source> {
    pub fn new(input: &'source str) -> Self {
        // count the characters on each line of the input
        let mut newlines = Vec::new();
        for (i, c) in input.char_indices() {
            if c == '\n' {
                newlines.push(i + 1);
            }
        }
        newlines.push(input.len());
        OatLexer {
            internal_lexer: OatToken::lexer(input).spanned(),
            nesting: 0,
            eof_reached: false,
            newlines,
        }
    }

    fn find_index_of_pos(&self, pos: usize) -> Location {
        let mut prev_newline = 0;
        for (i, &newline) in self.newlines.iter().enumerate() {
            if newline > pos {
                let diff = pos - prev_newline;
                return Location::new(i, diff);
            }
            prev_newline = newline;
        }
        Location::new(0, 0)
    }
}

impl<'source> Iterator for OatLexer<'source> {
    type Item = LexResult;

    fn next(&mut self) -> Option<Self::Item> {
        if self.eof_reached {
            return None;
        }
        match self.internal_lexer.next() {
            Some((token, range)) => {
                if token == Ok(OatToken::LBrace) {
                    self.nesting += 1;
                } else if token == Ok(OatToken::RBrace) {
                    self.nesting -= 1;
                }
                if self.nesting < 0 {
                    return Some(Err(LexicalError::UnbalancedBrackets(
                        Range(
                            token.unwrap().to_string(),
                            self.find_index_of_pos(range.start),
                            self.find_index_of_pos(range.end),
                        ),
                        self.nesting,
                    )));
                }
                Some(Ok((
                    self.find_index_of_pos(range.start),
                    token.unwrap(),
                    self.find_index_of_pos(range.end),
                )))
            }
            None => {
                self.eof_reached = true;
                Some(Ok((
                    Location::new(0, 0),
                    OatToken::Eof,
                    Location::new(0, 0),
                )))
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use crate::tokens::OatLexer;

    use super::OatToken;
    use super::OatToken::*;
    use logos::Logos;
    #[test]
    fn test_random_expr() {
        let source = "
        int program (int argc, string[] argv) {
            var x = 0;
            var i = 0;
          
            while (i < 10) {
              x = (x + i) * i;
              i = i + 1;
            }
          
            return x;
          }
          ";
        let lex = OatToken::lexer(source);
        let collected = lex.collect::<Vec<_>>();
        let solution = vec![
            TInt,
            Ident("program".to_owned()),
            LParen,
            TInt,
            Ident("argc".to_owned()),
            Comma,
            TString,
            LBracket,
            RBracket,
            Ident("argv".to_owned()),
            RParen,
            LBrace,
            Var,
            Ident("x".to_owned()),
            Eq,
            Int(0),
            Semi,
            Var,
            Ident("i".to_owned()),
            Eq,
            Int(0),
            Semi,
            While,
            LParen,
            Ident("i".to_owned()),
            Lt,
            Int(10),
            RParen,
            LBrace,
            Ident("x".to_owned()),
            Eq,
            LParen,
            Ident("x".to_owned()),
            Plus,
            Ident("i".to_owned()),
            RParen,
            Star,
            Ident("i".to_owned()),
            Semi,
            Ident("i".to_owned()),
            Eq,
            Ident("i".to_owned()),
            Plus,
            Int(1),
            Semi,
            RBrace,
            Return,
            Ident("x".to_owned()),
            Semi,
            RBrace,
        ];
        assert_eq!(
            collected
                .iter()
                .filter_map(|x| x.as_ref().ok())
                .collect::<Vec<_>>(),
            solution.iter().map(|x| x).collect::<Vec<_>>()
        );
        println!("{:?}", collected);
        println!(
            "{:?}",
            OatToken::lexer(source).spanned().collect::<Vec<_>>()
        );
    }

    #[test]
    fn test_random_expr_2() {
        let source = "
        int program (int argc, string[] argv) {
            var x = 0;
            var i = 0;
          
            while (i < 10) {
              x = (x + i) * i;
              i = i + 1;
            }
          
            return x;
          }
          ";
        let lex = OatLexer::new(source);
        let collected = lex.collect::<Vec<_>>();
        // assert_eq!(collected, solution);
        println!("{:?}", collected);
        // println!(
        //     "{:?}",
        //     OatToken::lexer(source).spanned().collect::<Vec<_>>()
        // );
    }
}
