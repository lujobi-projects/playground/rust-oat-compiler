#[cfg(test)]
mod tests {
    use ll::{backend::*, ll::*};
    use std::collections::HashMap;

    #[test]
    fn size_ty_tests() {
        use Ty::*;
        assert_eq!(size_ty(HashMap::new(), I1), 8);
        assert_eq!(size_ty(HashMap::new(), I64), 8);
        assert_eq!(size_ty(HashMap::new(), Ptr(Box::new(Void))), 8);
        assert_eq!(size_ty(HashMap::new(), Ptr(Box::new(I1))), 8);
        assert_eq!(size_ty(HashMap::new(), Array(3, Box::new(I64))), 24);
        assert_eq!(
            size_ty(
                HashMap::new(),
                Struct(vec![
                    Box::new(I64),
                    Box::new(I1),
                    Box::new(Ptr(Box::new(I8))),
                    Box::new(Ptr(Box::new(I64))),
                    Box::new(Array(10, Box::new(I1)))
                ])
            ),
            112
        );
        assert_eq!(
            size_ty(
                HashMap::from([(Tid("O".to_string()), I1), (Tid("S".to_string()), I64)]),
                Struct(vec![
                    Box::new(Namedt(Tid("O".to_string()))),
                    Box::new(Array(2, Box::new(Namedt(Tid("S".to_string())))))
                ])
            ),
            24
        );
    }
}
