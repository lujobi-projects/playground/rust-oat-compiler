pub use x86::{self, simulator};
mod commons;

#[cfg(test)]
mod hard {
    use anyhow::bail;
    use x86::*;

    fn factorial_iter(n: i64) -> x86::Prog {
        prog!(
            text!(
                main,
                ins!(Pushq, reg!(Rbp)),
                ins!(Movq, reg!(Rsp), reg!(Rbp)),
                ins!(Movq, lit!(1), reg!(Rax)),
                ins!(Movq, lit!(n), reg!(Rdi))
            ),
            text!(
                _loop,
                ins!(Cmpq, lit!(0), reg!(Rdi)),
                ins!(J, Eq, lbl!(exit)),
                ins!(Imulq, reg!(Rdi), reg!(Rax)),
                ins!(Decq, reg!(Rdi)),
                ins!(Jmp, lbl!(_loop))
            ),
            text!(
                exit,
                ins!(Movq, reg!(Rbp), reg!(Rsp)),
                ins!(Popq, reg!(Rbp)),
                ins!(Retq)
            )
        )
    }

    fn factorial_rec(n: i64) -> x86::Prog {
        prog!(
            text!(
                fac,
                ins!(Subq, lit!(8), reg!(Rsp)),
                ins!(Cmpq, lit!(1), reg!(Rdi)),
                ins!(J, Le, lbl!(exit)),
                ins!(Movq, reg!(Rdi), ind_reg!(Rsp)),
                ins!(Decq, reg!(Rdi)),
                ins!(Callq, lbl!(fac)),
                ins!(Imulq, ind_reg!(Rsp), reg!(Rax)),
                ins!(Addq, lit!(8), reg!(Rsp)),
                ins!(Retq)
            ),
            text!(
                exit,
                ins!(Movq, lit!(1), reg!(Rax)),
                ins!(Addq, lit!(8), reg!(Rsp)),
                ins!(Retq)
            ),
            text!(
                main,
                ins!(Movq, lit!(n), reg!(Rdi)),
                ins!(Callq, lbl!(fac)),
                ins!(Retq)
            )
        )
    }

    fn program_test(p: Prog, ans: i64) -> anyhow::Result<()> {
        let res = run(load(assemble(p)?));
        if res != ans {
            bail!("Expected {} but got {}", ans, res);
        }
        Ok(())
    }

    // Tests
    #[test]
    fn factorial_tests() {
        k9::assert_ok!(program_test(factorial_iter(6), 720));
        k9::assert_ok!(program_test(factorial_rec(6), 720));
    }
}
