use std::fs::File;
use std::io::prelude::*;
use std::io::Write;
use std::process::Command;

use crate::platform::*;

#[macro_export]
macro_rules! current_dir {
    ($res:expr) => {
        format!("{}/{}", env!("CARGO_MANIFEST_DIR"), $res)
    };
}

pub fn write_file(filename: &String, content: &str) -> anyhow::Result<()> {
    let mut file = File::create(filename)?;
    file.write_all(content.as_bytes())?;
    Ok(())
}

pub fn remove_file(filename: &String) -> anyhow::Result<()> {
    std::fs::remove_file(filename)?;
    Ok(())
}

pub fn read_file(filename: &str) -> anyhow::Result<String> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

pub fn run_executable(arg: &str, prog_file: &str) -> anyhow::Result<i32> {
    let mut cmd = Command::new(prog_file);
    cmd.arg(arg);
    sh(&mut cmd, Box::new(|_, i| Ok(i)))
}
pub fn run_executable_to_tmpfile(
    arg: &String,
    prog_file: &String,
    tmp: &String,
) -> anyhow::Result<i32> {
    let mut cmd = Command::new(prog_file);
    cmd.arg(arg).arg(">").arg(tmp).arg("2>&1");
    sh(&mut cmd, Box::new(|_, i| Ok(i)))
}

///
/// Runs a command and returns the exit code, stdout and stderr.
pub fn run_program(args: Vec<&str>, executable: &str) -> anyhow::Result<(i32, String, String)> {
    let mut cmd = Command::new(executable);
    let out = cmd.args(args).output()?;
    let str_std_out = String::from_utf8(out.stdout)?;
    let str_std_out = str_std_out.trim_end_matches('\n');
    let str_std_err = String::from_utf8(out.stderr)?;
    let str_std_err = str_std_err.trim_end_matches('\n');
    Ok((
        out.status.code().unwrap(),
        str_std_out.to_string(),
        str_std_err.to_string(),
    ))
}
