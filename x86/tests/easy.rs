pub use x86::{self, simulator};
#[macro_use]
mod commons;

#[cfg(test)]
mod easy_tests {

    use anyhow::bail;
    use x86::*;

    fn helloworld() -> x86::Prog {
        prog!(
            text!(
                foo,
                ins!(Xorq, reg!(Rax), reg!(Rax)),
                ins!(Movq, lit!(100), reg!(Rax)),
                ins!(Retq)
            ),
            text!(
                main,
                ins!(Xorq, reg!(Rax), reg!(Rax)),
                ins!(Movq, ind!(lbl_imm!(baz)), reg!(Rax)),
                ins!(Retq)
            ),
            data!(baz, quad!(99), asciz!("Hello, world!"))
        )
    }

    fn test_exec() -> x86::Exec {
        x86::Exec {
            entry: 0x400008,
            text_pos: 0x400000,
            data_pos: 0x400064,
            text_seg: vec![],
            data_seg: vec![],
        }
    }

    fn interp_cnd_test((fo, fs, fz): (bool, bool, bool), tru: Vec<Cnd>) -> anyhow::Result<()> {
        let flags = Flags { fo, fs, fz };
        let all = vec![Cnd::Eq, Cnd::Neq, Cnd::Gt, Cnd::Ge, Cnd::Lt, Cnd::Le];

        let tru_ = all
            .clone()
            .into_iter()
            .filter(|c| interp_cnd(*c, &flags))
            .collect::<Vec<_>>();
        let tmp = tru_.clone();
        let fls_ = all.clone().into_iter().filter(|c| !tmp.contains(c));

        for c in tru_ {
            if !tru.contains(&c) {
                bail!("o:{} s:{} f:{}, {} expected", fo, fs, fz, c);
            }
        }
        for c in fls_ {
            if !all
                .clone()
                .into_iter()
                .filter(|c| !tru.contains(c))
                .any(|x| x == c)
            {
                bail!("o:{} s:{} f:{}, {} not expected", fo, fs, fz, c);
            }
        }
        Ok(())
    }

    fn segfault_test(addr: x86::Quad) -> anyhow::Result<()> {
        if map_addr(addr).is_some() {
            bail!("Should have raised X86_segmentation_fault");
        }
        Ok(())
    }

    // Tests
    #[test]
    fn map_addr_tests() {
        k9::assert_equal!(map_addr(0x40FFF8), Some(65528));
        k9::assert_equal!(map_addr(0x4000FF), Some(255));
        k9::assert_equal!(map_addr(0x400000), Some(0));
        k9::assert_ok!(segfault_test(0x0000000000000000));
        k9::assert_ok!(segfault_test(0xFFFFFFFFFFFFFFFD_u64 as i64));
    }

    #[test]
    fn interp_cnd_tests() {
        use Cnd::*;
        k9::assert_ok!(interp_cnd_test((false, false, false), vec![Neq, Gt, Ge]));
        k9::assert_ok!(interp_cnd_test((false, false, true), vec![Eq, Le, Ge]));
        k9::assert_ok!(interp_cnd_test((false, true, false), vec![Neq, Le, Lt]));
        k9::assert_ok!(interp_cnd_test((false, true, true), vec![Eq, Le, Lt]));

        k9::assert_ok!(interp_cnd_test((true, false, false), vec![Neq, Le, Lt]));
        k9::assert_ok!(interp_cnd_test((true, false, true), vec![Eq, Le, Lt]));
        k9::assert_ok!(interp_cnd_test((true, true, false), vec![Neq, Gt, Ge]));
        k9::assert_ok!(interp_cnd_test((true, true, true), vec![Eq, Le, Ge]));
    }

    #[test]
    fn easy_assemble_tests() -> X86LiteResult<()> {
        k9::assert_equal!(assemble(helloworld())?.text_pos, 0x400000);
        k9::assert_equal!(assemble(helloworld())?.data_pos, 0x400030);
        Ok(())
    }

    #[test]
    fn easy_load_tests() {
        k9::assert_equal!(
            load(test_exec()).flags,
            Flags {
                fo: false,
                fs: false,
                fz: false
            }
        );
        k9::assert_equal!(load(test_exec()).regs[rind(&Reg::Rip)], 0x400008);
        k9::assert_equal!(load(test_exec()).regs[rind(&Reg::Rsp)], 0x40FFF8);
    }
}
