pub use x86::{self, simulator};
mod commons;

#[cfg(test)]
mod tests {
    use x86::*;

    fn sbyte_list(list: &[SByte], start: i64) -> Vec<SByte> {
        let mut result = vec![];
        result.reserve(8);
        for i in start..start + 8 {
            result.push(list[i as usize].clone());
        }
        result
    }

    fn helloworld() -> x86::Prog {
        prog!(
            text!(
                foo,
                ins!(Xorq, reg!(Rax), reg!(Rax)),
                ins!(Movq, lit!(100), reg!(Rax)),
                ins!(Retq)
            ),
            text!(
                main,
                ins!(Xorq, reg!(Rax), reg!(Rax)),
                ins!(Movq, ind!(lbl_imm!(baz)), reg!(Rax)),
                ins!(Retq)
            ),
            data!(baz, quad!(99), asciz!("Hello, world!"))
        )
    }

    fn test_exec() -> x86::Exec {
        x86::Exec {
            entry: 0x400008,
            text_pos: 0x400000,
            data_pos: 0x400064,
            text_seg: vec![],
            data_seg: vec![],
        }
    }

    fn test_machine(bs: Vec<SByte>) -> Mach {
        let mut mem = vec![SByte::Byte(0); MEM_SIZE as usize];
        mem.splice(0..bs.len(), bs);
        let mut regs = vec![0; NREGS as usize];
        regs[rind(&Reg::Rip)] = MEM_BOT;
        regs[rind(&Reg::Rsp)] = MEM_TOP - 8;
        Mach {
            mem,
            regs,
            flags: Flags {
                fo: false,
                fs: false,
                fz: false,
            },
        }
    }
    fn helloworld_dataseg() -> Vec<SByte> {
        vec![
            cbyte!('c'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('\x00'),
            cbyte!('H'),
            cbyte!('e'),
            cbyte!('l'),
            cbyte!('l'),
            cbyte!('o'),
            cbyte!(','),
            cbyte!(' '),
            cbyte!('w'),
            cbyte!('o'),
            cbyte!('r'),
            cbyte!('l'),
            cbyte!('d'),
            cbyte!('!'),
            cbyte!('\x00'),
        ]
    }

    fn helloworld_textseg() -> Vec<SByte> {
        sbyte_insns!(
            ins!(Xorq, reg!(Rax), reg!(Rax)),
            ins!(Movq, lit!(100), reg!(Rax)),
            ins!(Retq),
            ins!(Xorq, reg!(Rax), reg!(Rax)),
            ins!(Movq, ind!(Imm::Lit(0x400030)), reg!(Rax)),
            ins!(Retq)
        )
    }

    fn cc_test(
        s: String,
        n: u64,
        mut m: Mach,
        (fo, fs, fz): (bool, bool, bool),
        f: &mut dyn FnMut(Mach) -> bool,
    ) -> Result<(), String> {
        m.flags = Flags { fo, fs, fz };
        for _ in 0..n {
            step(&mut m);
        }
        if !f(m) {
            return Err(s);
        }
        Ok(())
    }

    fn cs_test(n: u64, m: Mach, (fo_, fs_, fz_): (bool, bool, bool)) -> Result<(), String> {
        cc_test(
            format!("expected OF: {fo_} SF: {fs_} ZF: {fz_}"),
            n,
            m,
            (!fo_, !fs_, !fz_),
            &mut |m: Mach| m.flags.fo == fo_ && m.flags.fs == fs_ && m.flags.fz == fz_,
        )
    }
    fn cso_test(n: u64, m: Mach, fo_: bool) -> Result<(), String> {
        cc_test(
            format!("expected OF: {fo_}"),
            n,
            m,
            (fo_, false, false),
            &mut (move |m: Mach| m.flags.fo == fo_),
        )
    }
    fn csi_test(n: u64, m: Mach) -> Result<(), String> {
        cc_test(
            "expected TTT ccodes".to_string(),
            n,
            m,
            (true, true, true),
            &mut |m: Mach| m.flags.fo && m.flags.fs && m.flags.fz,
        )
    }

    fn undefined_sym_test(p: Prog) -> Result<(), String> {
        match assemble(p) {
            Ok(_) => Err("expected error".to_string()),
            Err(e) => match e {
                x86::X86liteSegfault::UndefinedSym(_) => Ok(()),
                _ => Err(format!("expected undefined symbol error, got {:?}", e)),
            },
        }
    }

    fn machine_test(
        s: &str,
        n: u64,
        mut m: Mach,
        f: &mut dyn std::ops::FnMut(x86::Mach) -> bool,
    ) -> Result<(), String> {
        for _ in 0..n {
            step(&mut m);
        }
        if !f(m) {
            return Err(format!("expected {}", s));
        }
        Ok(())
    }

    fn mov_mr() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(42), reg!(Rax)),
            ins!(Movq, reg!(Rax), stack_offset!(-8))
        ))
    }

    fn subq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Subq, lit!(1), reg!(Rax)),
            ins!(Subq, reg!(Rax), reg!(Rbx)),
            ins!(Subq, reg!(Rbx), stack_offset!(0))
        ))
    }

    fn andq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(2), reg!(Rax)),
            ins!(Movq, lit!(3), reg!(Rbx)),
            ins!(Movq, lit!(255), reg!(Rcx)),
            ins!(Movq, lit!(1), stack_offset!(0)),
            ins!(Andq, reg!(Rax), reg!(Rax)),
            ins!(Andq, lit!(2), reg!(Rax)),
            ins!(Andq, reg!(Rax), reg!(Rbx)),
            ins!(Andq, stack_offset!(0), reg!(Rcx))
        ))
    }

    fn negq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(42), reg!(Rax)),
            ins!(Movq, lit!(-24), stack_offset!(0)),
            ins!(Movq, lit!(i64::MIN), reg!(Rbx)),
            ins!(Negq, reg!(Rax)),
            ins!(Negq, stack_offset!(0)),
            ins!(Negq, reg!(Rbx))
        ))
    }

    fn shlq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(1), reg!(Rax)),
            ins!(Movq, lit!(2), stack_offset!(0)),
            ins!(Movq, lit!(3), reg!(Rcx)),
            ins!(Shlq, lit!(2), reg!(Rax)),
            ins!(Shlq, reg!(Rcx), stack_offset!(0))
        ))
    }

    fn imul() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(2), reg!(Rax)),
            ins!(Movq, lit!(22), reg!(Rbx)),
            ins!(Imulq, reg!(Rbx), reg!(Rax))
        ))
    }

    fn pushq() -> Mach {
        test_machine(sbyte_insns!(ins!(Pushq, lit!(42))))
    }

    fn popq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Addq, lit!(-8), reg!(Rsp)),
            ins!(Movq, lit!(42), stack_offset!(0)),
            ins!(Popq, reg!(Rax))
        ))
    }

    fn cmpq() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(4), reg!(Rax)),
            ins!(Movq, lit!(2), stack_offset!(0)),
            ins!(Cmpq, lit!(1), reg!(Rax)),
            ins!(Cmpq, reg!(Rax), reg!(Rbx)),
            ins!(Cmpq, reg!(Rbx), stack_offset!(0))
        ))
    }

    fn cc_add_1() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0xFFFFFFFFFFFFFFFF_u64 as i64), reg!(Rax)),
            ins!(Addq, lit!(1), reg!(Rax))
        ))
    }
    fn cc_add_2() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0xFFFFFFFFFFFFFFFF_u64 as i64), reg!(Rax)),
            ins!(Addq, lit!(0xFFFFFFFFFFFFFFFF_u64 as i64), reg!(Rax))
        ))
    }
    fn cc_add_3() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0x7FFFFFFFFFFFFFFF_u64 as i64), reg!(Rax)),
            ins!(Addq, lit!(42), reg!(Rax))
        ))
    }
    fn cc_add_4() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0x9000000000000000_u64 as i64), reg!(Rax)),
            ins!(Addq, lit!(0xA000000000000000_u64 as i64), reg!(Rax))
        ))
    }
    fn cc_neg_1() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(i64::MIN), reg!(Rbx)),
            ins!(Negq, reg!(Rbx))
        ))
    }
    fn cc_neg_2() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(1), reg!(Rax)),
            ins!(Negq, reg!(Rax))
        ))
    }
    fn cc_cmp_1() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0), reg!(Rax)),
            ins!(Cmpq, lit!(0x8000000000000000_u64 as i64), reg!(Rax))
        ))
    }
    fn cc_cmp_2() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0x8000000000000000_u64 as i64), reg!(Rax)),
            ins!(Cmpq, lit!(0), reg!(Rax))
        ))
    }
    fn cc_imul_1() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(-1), reg!(Rax)),
            ins!(Imulq, lit!(-1), reg!(Rax))
        ))
    }
    fn cc_and() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0x0F0F0F0F), reg!(Rax)),
            ins!(Andq, lit!(0xF0F0F0F0), reg!(Rax))
        ))
    }
    fn cc_or() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0xFFFFFFF), reg!(Rax)),
            ins!(Orq, lit!(0xF0F0F0F0), reg!(Rax))
        ))
    }
    fn cc_set() -> Mach {
        test_machine(sbyte_insns!(ins!(Set, Neq, reg!(Rax))))
    }
    fn cc_push() -> Mach {
        test_machine(sbyte_insns!(ins!(Pushq, lit!(0))))
    }
    fn cc_pop() -> Mach {
        test_machine(sbyte_insns!(ins!(Popq, reg!(Rax))))
    }
    fn cc_ret() -> Mach {
        test_machine(sbyte_insns!(ins!(Retq)))
    }
    fn cc_mov() -> Mach {
        test_machine(sbyte_insns!(ins!(Movq, lit!(0), reg!(Rax))))
    }
    fn cc_jmp() -> Mach {
        test_machine(sbyte_insns!(ins!(Jmp, lit!(0x400008))))
    }
    fn cc_js() -> Mach {
        test_machine(sbyte_insns!(ins!(J, Neq, lit!(0x400008))))
    }
    fn cc_jf() -> Mach {
        test_machine(sbyte_insns!(ins!(J, Eq, lit!(0x400008))))
    }
    fn cc_call() -> Mach {
        test_machine(sbyte_insns!(ins!(Callq, lit!(0x400008))))
    }
    fn cc_lea() -> Mach {
        test_machine(sbyte_insns!(
            ins!(Movq, lit!(0x400600), reg!(Rax)),
            ins!(Movq, lit!(0x408000), reg!(Rcx)),
            ins!(Leaq, ind_reg!(Rax), reg!(R08)),
            ins!(Movq, reg!(R08), ind_reg!(Rcx))
        ))
    }
    // Tests

    #[test]
    fn medium_assemble_tests() -> X86LiteResult<()> {
        k9::assert_equal!(assemble(helloworld())?.text_seg, helloworld_textseg());
        k9::assert_ok!(undefined_sym_test(prog!(text!(foo, ins!(Retq)))));
        k9::assert_equal!(assemble(helloworld())?.data_seg, helloworld_dataseg());
        k9::assert_ok!(undefined_sym_test(prog!(text!(
            main,
            ins!(Jmp, lbl!(loop_)),
            ins!(Retq)
        ))));
        Ok(())
    }

    #[test]
    fn medium_load_tests() {
        let m = load(test_exec());
        k9::assert_equal!(int64_of_sbytes(sbyte_list(&m.mem, 0xfff8)), EXIT_ADDR);
    }

    #[test]
    fn functionality_tests() {
        k9::assert_ok!(machine_test(
            "rax=42",
            1,
            test_machine(sbyte_insns!(ins!(Movq, lit!(42), reg!(Rax)))),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 42
        ));
        k9::assert_ok!(machine_test(
            "rax=rbx=*66528=1",
            3,
            test_machine(sbyte_insns!(
                ins!(Addq, lit!(1), reg!(Rax)),
                ins!(Addq, reg!(Rax), reg!(Rbx)),
                ins!(Addq, reg!(Rbx), stack_offset!(0))
            )),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 1
                && m.regs[rind(&Reg::Rbx)] == 1
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == 1
        ));
    }

    #[test]
    fn instruction_tests() {
        k9::assert_ok!(machine_test(
            "*65520=42",
            2,
            mov_mr(),
            &mut |m: Mach| int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 16)) == 42
        ));
        k9::assert_ok!(machine_test(
            "rax=*65528=-1L; rbx=1",
            3,
            subq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == -1
                && m.regs[rind(&Reg::Rbx)] == 1
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == -1
        ));
        k9::assert_ok!(machine_test(
            "rax=2 rbx=2 rcx=1 *65528=1",
            8,
            andq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 2
                && m.regs[rind(&Reg::Rbx)] == 2
                && m.regs[rind(&Reg::Rcx)] == 1
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == 1
        ));
        k9::assert_ok!(machine_test(
            "rax=-42 rbx=min_int64 *65528=24",
            6,
            negq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == -42
                && m.regs[rind(&Reg::Rbx)] == i64::MIN
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == 24
        ));
        k9::assert_ok!(machine_test(
            "rax=4 *65528=16",
            5,
            shlq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 4
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == 12
        ));
        k9::assert_ok!(machine_test(
            "rax=44 *65528=2",
            3,
            imul(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 44
        ));
        k9::assert_ok!(machine_test(
            "rsp=4 *65520=2A",
            1,
            pushq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rsp)] == 0x0040FFF0
                && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 16)) == 0x2A
        ));
        k9::assert_ok!(machine_test(
            "rsp=4259832 rax=2A",
            3,
            popq(),
            &mut |m: Mach| m.regs[rind(&Reg::Rax)] == 0x2A && m.regs[rind(&Reg::Rsp)] == 0x0040FFF8
        ));
        k9::assert_ok!(machine_test("rax=4 rbx=0", 5, cmpq(), &mut |m: Mach| m
            .regs[rind(&Reg::Rax)]
            == 4
            && m.regs[rind(&Reg::Rbx)] == 0
            && int64_of_sbytes(sbyte_list(&m.mem, MEM_SIZE - 8)) == 2));
    }

    #[test]
    fn condition_flag_set_tests() {
        k9::assert_ok!(cs_test(2, cc_add_1(), (false, false, true)));
        k9::assert_ok!(cs_test(2, cc_add_2(), (false, true, false)));
        k9::assert_ok!(cs_test(2, cc_add_3(), (true, true, false)));
        k9::assert_ok!(cs_test(2, cc_add_4(), (true, false, false)));
        k9::assert_ok!(cs_test(2, cc_neg_1(), (true, true, false)));
        k9::assert_ok!(cs_test(2, cc_neg_2(), (false, true, false)));
        k9::assert_ok!(cs_test(2, cc_cmp_1(), (true, true, false)));
        k9::assert_ok!(cs_test(2, cc_cmp_2(), (false, true, false)));
        k9::assert_ok!(cso_test(2, cc_imul_1(), false));
        k9::assert_ok!(cs_test(2, cc_and(), (false, false, true)));
        k9::assert_ok!(cs_test(2, cc_or(), (false, false, false)));
        k9::assert_ok!(csi_test(1, cc_push()));
        k9::assert_ok!(csi_test(1, cc_pop()));
        k9::assert_ok!(csi_test(1, cc_set()));
        k9::assert_ok!(csi_test(1, cc_ret()));
        k9::assert_ok!(csi_test(1, cc_mov()));
        k9::assert_ok!(csi_test(1, cc_jmp()));
        k9::assert_ok!(csi_test(1, cc_js()));
        k9::assert_ok!(csi_test(1, cc_jf()));
        k9::assert_ok!(csi_test(1, cc_call()));
        k9::assert_ok!(csi_test(3, cc_lea()));
    }
}
