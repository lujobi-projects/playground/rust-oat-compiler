#[macro_use]
extern crate lalrpop_util;
lalrpop_mod!(#[allow(clippy::all)] pub oat_parser);

pub mod ast;
pub mod frontend;
pub mod tokens;
pub mod utils;

#[cfg(test)]
mod tests {
    use crate::oat_parser;
    use crate::tokens;

    fn compile_and_run(prog: &str) -> i64 {
        let lexer2 = tokens::make_tokenizer(prog);
        println!(
            "{}",
            lexer2
                .collect::<Vec<_>>()
                .iter()
                .map(|t| format!("{:?}", t))
                .collect::<Vec<_>>()
                .join("\n")
        );
        let lexer = tokens::make_tokenizer(prog);
        let program = oat_parser::progParser::new().parse("no_file", lexer).expect("Oh no");
        println!("{:?}", program);
        1
    }
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[test]
    fn test_parser() {
        let source = "
        int program (int argc, string[] argv) {
            return 17 + 18;
        }          
        ";
        assert_eq!(compile_and_run(source), 1);
    }
}
