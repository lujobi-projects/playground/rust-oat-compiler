#[cfg(test)]
mod tests {

    use ll::{self, backend, ll::*, ll_parser, tokens};
    use utils::{current_dir, driver, platform};

    const OUTPUT_PATH: &str = "ll_output";

    pub fn parse_ll_file(filename: &str) -> anyhow::Result<Prog> {
        let content = driver::read_file(&current_dir!(filename))?;
        let lexer = tokens::make_tokenizer(&content);
        let p = ll_parser::progParser::new().parse(lexer)?;
        Ok(p)
    }

    fn exec_e2e_ast(ll_ast: Prog, args: &str, extra_files: Vec<&String>) -> anyhow::Result<i32> {
        let dot_s_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "test", ".s")?;
        let exec_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "exec", "")?;
        let asm_ast = backend::compile_prog(ll_ast);
        let asm_str = asm_ast.to_string();

        driver::write_file(&dot_s_file_name, &asm_str)?;

        platform::link(
            vec![&dot_s_file_name]
                .into_iter()
                .chain(extra_files)
                .collect(),
            &exec_file_name,
        )?;
        let res = driver::run_executable(args, &exec_file_name)?;

        driver::remove_file(&dot_s_file_name)?;
        driver::remove_file(&exec_file_name)?;

        println!("** Executable exited with: {}", res);
        Ok(res)
    }

    fn exec_e2e_file(path: &str, args: &str) -> anyhow::Result<i32> {
        let ll_ast = parse_ll_file(path)?;
        exec_e2e_ast(ll_ast, args, vec![])
    }

    fn io_test(path: &str, args: Vec<&str>) -> anyhow::Result<String> {
        let ll_ast = parse_ll_file(path)?;
        let dot_s_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "test", ".s")?;
        let exec_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "exec", "")?;
        let asm_ast = backend::compile_prog(ll_ast);
        let asm_str = asm_ast.to_string();

        driver::write_file(&dot_s_file_name, &asm_str)?;
        platform::link(
            vec![&dot_s_file_name, &current_dir!("cinterop.c")],
            &exec_file_name,
        )?;

        let res = driver::run_program(args, &exec_file_name)?;
        driver::remove_file(&dot_s_file_name)?;
        driver::remove_file(&exec_file_name)?;

        println!("** Executable output: {}", res.1);
        Ok(res.1)
    }
    // TODO find out what todo
    // fn c_link_test(c_files: Vec<String>, path: String, args: Vec<String>) -> anyhow::Result<i32> {
    //     let ll_ast = parse_ll_file(&path)?;
    //     let dot_s_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "test", ".s")?;
    //     let exec_file_name = platform::gen_name(&current_dir!(OUTPUT_PATH), "exec", "")?;
    //     let asm_ast = backend::compile_prog(ll_ast);
    //     let asm_str = asm_ast.to_string();

    //     driver::write_file(&dot_s_file_name, &asm_str)?;
    //     platform::link(
    //         vec![dot_s_file_name.clone()]
    //             .into_iter()
    //             .chain(c_files)
    //             .collect(),
    //         &exec_file_name,
    //     )?;

    //     let args = args.join(" ");
    //     let res = driver::run_executable(&args, &exec_file_name)?;

    //     driver::remove_file(&dot_s_file_name)?;
    //     driver::remove_file(&exec_file_name)?;

    //     println!("** Executable exited with: {}", res);
    //     Ok(res)
    // }

    macro_rules! assert_res_equal {
        ($res:expr, $left:expr) => {
            match $res {
                Ok(res) => k9::assert_equal!(res, $left),
                Err(_) => k9::assert_ok!($res),
            }
        };
    }

    fn execute(path: &str) -> anyhow::Result<i32> {
        exec_e2e_file(path, "")
    }

    #[test]
    fn binop_tests() {
        assert_res_equal!(execute("llprograms/add.ll"), 14);
        assert_res_equal!(execute("llprograms/sub.ll"), 1);
        assert_res_equal!(execute("llprograms/mul.ll"), 45);
        assert_res_equal!(execute("llprograms/and.ll"), 0);
        assert_res_equal!(execute("llprograms/or.ll"), 1);
        assert_res_equal!(execute("llprograms/xor.ll"), 0);
        assert_res_equal!(execute("llprograms/shl.ll"), 168);
        assert_res_equal!(execute("llprograms/lshr.ll"), 10);
        assert_res_equal!(execute("llprograms/ashr.ll"), 5);
    }

    #[test]
    fn calling_convention_tests() {
        assert_res_equal!(execute("llprograms/call.ll"), 42);
        assert_res_equal!(execute("llprograms/call1.ll"), 17);
        assert_res_equal!(execute("llprograms/call2.ll"), 19);
        assert_res_equal!(execute("llprograms/call3.ll"), 34);
        assert_res_equal!(execute("llprograms/call4.ll"), 34);
        assert_res_equal!(execute("llprograms/call5.ll"), 24);
        assert_res_equal!(execute("llprograms/call6.ll"), 26);
    }
    #[test]
    fn memory_tests() {
        assert_res_equal!(execute("llprograms/alloca1.ll"), 17);
        assert_res_equal!(execute("llprograms/alloca2.ll"), 17);
        assert_res_equal!(execute("llprograms/global1.ll"), 12);
    }
    #[test]
    fn terminator_tests() {
        assert_res_equal!(execute("llprograms/return.ll"), 0);
        assert_res_equal!(execute("llprograms/return42.ll"), 42);
        assert_res_equal!(execute("llprograms/br1.ll"), 9);
        assert_res_equal!(execute("llprograms/br2.ll"), 17);
        assert_res_equal!(execute("llprograms/cbr1.ll"), 7);
        assert_res_equal!(execute("llprograms/cbr2.ll"), 9);
        assert_res_equal!(execute("llprograms/duplicate_lbl.ll"), 1);
    }
    #[test]
    fn bitcast_tests() {
        assert_res_equal!(execute("llprograms/bitcast1.ll"), 3);
    }
    #[test]
    fn gep_tests() {
        assert_res_equal!(execute("llprograms/gep1.ll"), 6);
        assert_res_equal!(execute("llprograms/gep2.ll"), 4);
        assert_res_equal!(execute("llprograms/gep3.ll"), 1);
        assert_res_equal!(execute("llprograms/gep4.ll"), 2);
        assert_res_equal!(execute("llprograms/gep5.ll"), 4);
        assert_res_equal!(execute("llprograms/gep6.ll"), 7);
        assert_res_equal!(execute("llprograms/gep7.ll"), 7);
        assert_res_equal!(execute("llprograms/gep8.ll"), 2);
    }
    #[test]
    fn io_tests() {
        assert_res_equal!(io_test("llprograms/helloworld.ll", vec![]), "hello, world!");
        assert_res_equal!(
            io_test("llprograms/string1.ll", vec![]),
            "hello, world!hello, world!"
        );
        assert_res_equal!(io_test("llprograms/callback1.ll", vec![]), "38");
        assert_res_equal!(io_test("llprograms/args1.ll", vec!["hello"]), "argc < 3");
        assert_res_equal!(
            io_test("llprograms/args1.ll", vec!["hello", "compilerdesign"]),
            "hellocompilerdesign"
        );
        assert_res_equal!(
            io_test(
                "llprograms/args1.ll",
                vec!["hello", "compilerdesign", "foo"]
            ),
            "argc > 3"
        );
    }
    #[test]
    fn large_tests() {
        assert_res_equal!(execute("llprograms/list1.ll"), 3);
        assert_res_equal!(execute("llprograms/cbr.ll"), 42);
        assert_res_equal!(execute("llprograms/factorial.ll"), 120);
        assert_res_equal!(execute("llprograms/factrect.ll"), 120);
        assert_res_equal!(execute("llprograms/duplicate_factorial.ll"), 240);
    }
}
